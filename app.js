(function () {
    'use strict';
    var Promise = require('bluebird');
    var app = require('express')();
    var kraken = require('kraken-js');

    module.exports = new Promise(function (resolve, reject) {

        app.listenAsync = function (port, ip) {
            return new Promise(function (resolve, reject) {
                var server = app.listen(port, ip, function (err) {
                    if (err) reject(err);
                    else resolve(server);
                });
            });
        };

        app.use(kraken({
            onconfig: function (config, next) {
                // Callback for Index.js
                resolve([config, app]);
                next(null, config);
            }
        }));
    });

})();
